const assets =[
    
    "index.html",
    "js/sb-admin-2.min.js",
    "js/sb-admin-2.js",
    "css/sb-admin-2.min.css",
    "css/sb-admin-2.css"
]


const statDevSBA = "dev-sb-admin"

self.addEventListener("install", (installEvent)  => {
    installEvent.waitUntil(
        caches.open(statDevSBA).then(
            (cache) => cache.addAll(assets)));
 }
);

self.addEventListener("fetch", (fetchEvent)  => {
    console.log('fetch url capturada:', fetchEvent.request.url);
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(
            (cachedResponse) => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                return fetch(fetchEvent.request);

        }),
    );
});

